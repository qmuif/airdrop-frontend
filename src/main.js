import { createApp } from 'vue';
import Vue3TouchEvents from 'vue3-touch-events';
import App from './App';
import './registerServiceWorker';
import router from './router';
import VClickOutside from './components/helpers/VClickOutside';

const app = createApp(App);
app.directive('click-outside', VClickOutside);
app.use(router).use(Vue3TouchEvents).mount('#app');
