import {
  computed, onMounted, onUnmounted, ref
} from 'vue';

export default function useBreakpoints() {
  const windowWidth = ref(window.innerWidth);

  // eslint-disable-next-line no-return-assign
  const onWidthChange = () => windowWidth.value = window.innerWidth;
  onMounted(() => window.addEventListener('resize', onWidthChange));
  onUnmounted(() => window.removeEventListener('resize', onWidthChange));

  // eslint-disable-next-line consistent-return,vue/return-in-computed-property
  const type = computed(() => {
    if (windowWidth.value < 576) return 'xs';
    if (windowWidth.value > 576 && windowWidth.value <= 768) return 'sm';
    if (windowWidth.value > 768 && windowWidth.value <= 960) return 'lg';
    if (windowWidth.value > 960 && windowWidth.value <= 1200) return 'xl';
    if (windowWidth.value > 1200) return 'xxl';
  });

  const width = computed(() => windowWidth.value);

  return { width, type };
}
