export default {
  mounted(el, binding) {
    // eslint-disable-next-line no-param-reassign
    el.listener = (event) => {
      if (!el.contains(event.target)) {
        binding.value();
      }
    };
    document.addEventListener('click', el.listener);
  },
  unmounted(el) {
    document.removeEventListener('click', el.listener, false);
  }
};
