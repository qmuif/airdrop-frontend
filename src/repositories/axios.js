import axios from 'axios';

axios.defaults.baseURL = process.env.NODE_ENV === 'production' // если сборка для продакшена
  ? 'http://wactrum.ru:8080/' // url обычный
  : 'http://localhost:8080/'; // url локальный

export default axios;
