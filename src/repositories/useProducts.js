import { provide, ref } from 'vue';
import axios from './axios';

export default function useProducts() {
  const landingProducts = ref([]);
  const isProductLoading = ref(true);
  const loadLandingProducts = async () => {
    try {
      isProductLoading.value = true;
      const response = await axios.get('/landing');
      landingProducts.value = response.data;
    } catch (e) {
      // eslint-disable-next-line no-alert
      alert('Ошибка');
    } finally {
      isProductLoading.value = false;
    }
  };
  provide('landingProducts', landingProducts);
  provide('isProductLoading', isProductLoading);
  return {
    landingProducts, isProductLoading, loadLandingProducts
  };
}
