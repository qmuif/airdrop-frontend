import { provide, ref } from 'vue';
import axios from './axios';

export default function useNews() {
  const news = ref([]);
  const isNewsLoading = ref(true);
  const loadNews = async () => {
    try {
      isNewsLoading.value = true;
      const response = await axios.get('/news');
      news.value = response.data.data;
    } catch (e) {
      // eslint-disable-next-line no-alert
      alert('Ошибка');
    } finally {
      isNewsLoading.value = false;
    }
  };
  provide('news', news);
  provide('isNewsLoading', isNewsLoading);
  return {
    news, isNewsLoading, loadNews
  };
}
